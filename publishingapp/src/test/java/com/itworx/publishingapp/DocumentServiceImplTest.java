package com.itworx.publishingapp;

import com.itworx.publishingapp.entity.Document;
import com.itworx.publishingapp.repository.DocumentRepository;
import com.itworx.publishingapp.service.impl.DocumentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class DocumentServiceImplTest {


    @InjectMocks
    private DocumentServiceImpl documentService;

    @Mock
    private DocumentRepository repository;

    @Test
    public void testGetAllDocuments(){

        List<Document> documents = new ArrayList<>();
        documents.add(new Document("Document1", "Document1 Description"));
        given(this.repository.findAll()).willReturn(documents);

        List<Document> docs = documentService.getAllDocuments();
        assertTrue(docs != null &&
                docs.stream().anyMatch(doc -> doc.getTitle().equalsIgnoreCase("Document1")));
    }
}
