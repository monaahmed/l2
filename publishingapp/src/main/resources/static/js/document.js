$(document).ready(function () {
    $("#upload-form").submit(function (event) {
        event.preventDefault();
        doValidate();
    });
});

function doValidate() {
    var title = $("#title").val();
    var params = { title: title };
    $.ajax({
        type: "POST",
        url: "/document/unique",
        data : params,
        success: function (data) {
            if(data != ""){
                $("#feedback").css("display", "block");
                $('#feedback').html(data);
            }else{$("#feedback").css("display", "none");}
        },
        error: function (e) {
            console.log("Error: ", e);
        }
    });
}