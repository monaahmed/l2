package com.itworx.publishingapp.repository;

import com.itworx.publishingapp.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query("select d from Document d join d.topics t where t.id = ?1")
    List<Document> findDocumentsByTopic(Long topicId);

    @Query("select d from Document d join d.topics t where t.id = ?1 and d.publisher = ?2")
    List<Document> findDocumentsByTopicAndPublisher(Long topicId, String email);

    boolean existsByTitle(String title);
}