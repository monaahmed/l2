package com.itworx.publishingapp.repository;

import com.itworx.publishingapp.entity.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TopicRepository extends JpaRepository<Topic, Long> {

    @Query("select t from Topic t order by t.name")
    List<Topic> findAllTopics();

    @Query("select distinct t from Topic t join t.documents d where d.publisher=?1 order by t.name")
    List<Topic> findTopicsByPublisher(String email);
}