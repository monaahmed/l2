package com.itworx.publishingapp.web.controller;

import com.itworx.publishingapp.entity.Document;
import com.itworx.publishingapp.entity.Topic;
import com.itworx.publishingapp.service.DocumentService;
import com.itworx.publishingapp.service.TopicService;
import com.itworx.publishingapp.web.model.UploadedDocument;
import com.itworx.publishingapp.web.validator.FileValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private FileValidator fileValidator;

    private static final Logger logger = LoggerFactory.getLogger(DocumentController.class);

    @ModelAttribute
    public void init(Model model) {
        List<Topic> topics = topicService.getAllTopics();
        model.addAttribute("topics", topics);
    }

    @GetMapping("/publisher/document/publish")
    public String publish(UploadedDocument uploadedDocument) {
        return "addDocument";
    }

    @PostMapping("/publisher/document/upload")
    public String upload(@Valid UploadedDocument document, BindingResult result) {
        fileValidator.validate(document.getFile(), result);
        if(result.hasErrors()){
            return "addDocument";
        }
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        document.setPublisher(username);
        documentService.publish(convertToEntity(document));
        return "redirect:/publisher/topics";
    }

    @GetMapping("/publisher/document/edit")
    public ModelAndView edit(@RequestParam String id, Model model) {
        Optional<Document> document = documentService.getDocumentDetails(Long.parseLong(id));
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if(!currentUser.equals(document.get().getPublisher())){
            return new ModelAndView("redirect:/publisher/topics");
        }
        return new ModelAndView("editDocument").
                addObject("uploadedDocument", convertToUploadedDocument(document.get()));
    }

    @PostMapping("/publisher/document/update")
    public String update(@Valid UploadedDocument document, BindingResult result) {
        if(result.hasErrors()){
            return "editDocument";
        }
        Optional<Document> entity = documentService.getDocumentDetails(document.getId());
        documentService.updateDocument(convertToEntity(document, entity.get()));
        return "redirect:/publisher/topics";
    }

    @GetMapping("/publisher/document/delete")
    public String delete(@RequestParam String id) {
        Optional<Document> document = documentService.getDocumentDetails(Long.parseLong(id));
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if(!currentUser.equals(document.get().getPublisher())){
            return "redirect:/publisher/topics";
        }
        documentService.deleteDocument(Long.parseLong(id));
        return "redirect:/publisher/topics";
    }

    @GetMapping("/publisher/documents")
    public ModelAndView listDocuments(HttpServletRequest request, @RequestParam String tid) {
        Long topicId = Long.parseLong(tid);
        String username = request.getUserPrincipal().getName();
        List<Document> documents = documentService.getDocumentsByTopicAndPublisher(topicId, username);
        String topicName = topicService.getTopicNameById(topicId);
        return new ModelAndView("documents").addObject("documents", documents).addObject(
                "topicName", topicName);
    }

    @GetMapping("/consumer/documents")
    public ModelAndView listAllDocuments(@RequestParam String tid) {
        Long topicId = Long.parseLong(tid);
        List<Document> documents = documentService.getDocumentsByTopic(topicId);
        String topicName = topicService.getTopicNameById(topicId);
        return new ModelAndView("documents").addObject("documents", documents).addObject(
                "topicName", topicName);
    }

    @GetMapping("/download")
    public void getDocumentContentById(HttpServletResponse response, @RequestParam Long id) {
        Optional<Document> document = documentService.getDocumentDetails(id);
        response.setHeader("Content-Type", document.get().getContentType());
        response.setHeader("Content-Disposition", "attachment;filename=\"" + document.get().getFileName());
        try {
            response.getOutputStream().write(document.get().getContent());
            response.flushBuffer();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @PostMapping("/document/unique")
    public @ResponseBody
    String isUnique(@RequestParam("title") String title) {
        String result = "";
        boolean exists = documentService.existsByTitle(title);
        if (exists) {
            result = "title already exists.";
        }
        return result;
    }

    private Document convertToEntity(UploadedDocument document) {
        return convertToEntity(document, null);
    }

    private Document convertToEntity(UploadedDocument document, Document entity) {
        if(entity == null) {
            entity = new Document(document.getTitle(), document.getDescription());
            entity.setPublisher(document.getPublisher());
        }else{
            entity.setTitle(document.getTitle());
            entity.setDescription(document.getDescription());
        }
        entity.setLastUpdatedAt(new Date());

        if(document.getFile().getSize() != 0) {
            entity.setContentType(document.getContentType());
            entity.setFileName(document.getFileName());
            try {
                entity.setContent(document.getContent());
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }

        entity.getTopics().clear();
        for(String tid : document.getSelectedTopics()) {
            Optional<Topic> topic = topicService.getTopicById(Long.parseLong(tid));
            entity.getTopics().add(topic.get());
            topic.get().getDocuments().add(entity);
        }

        return entity;
    }

    private UploadedDocument convertToUploadedDocument(Document entity) {
        UploadedDocument document = new UploadedDocument(entity.getId(), entity.getTitle(), entity.getDescription());
        document.setPublisher(entity.getPublisher());
        document.setFileName(entity.getFileName());
        document.setContentType(entity.getContentType());
        document.setContent(entity.getContent());
        for(Topic topic : entity.getTopics()) {
            document.getTopics().add(topic);
            document.getSelectedTopics().add(topic.getName());
        }
        return document;
    }

}

