package com.itworx.publishingapp.web.controller;

import com.itworx.publishingapp.entity.Role;
import com.itworx.publishingapp.entity.User;
import com.itworx.publishingapp.service.UserService;
import com.itworx.publishingapp.web.model.UserRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;

	@GetMapping("/login")
	public String login() { return "login";	}

	@GetMapping("/")
	public String home(HttpServletRequest request) {
		if(request.isUserInRole(Role.PUBLISHER.name())){
			return "redirect:/publisher/document/publish";
		}
		return "redirect:/consumer/topics";
	}

	@GetMapping("/register")
	public String register(UserRegistration userRegistration) {
		return "registration";
	}

	@PostMapping("/register")
	public String processRegister(@Valid UserRegistration userRegistration, BindingResult result) {
		if (result.hasErrors()) {
			return "registration";
		}
		String encodedPassword = bCryptPasswordEncoder.encode(userRegistration.getPassword());
		User newUser = new User(userRegistration.getUsername(), encodedPassword, userRegistration.getRole());
		userService.register(newUser);
		return "redirect:/";
	}

	@PostMapping("/register/unique")
	public @ResponseBody String isUnique(@RequestParam("username") String username) {
		String result = "";
		boolean exists = userService.existsByEmail(username);
		if (exists) {
			result = "username already exists.";
		}
		return result;
	}

}
