package com.itworx.publishingapp.web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileValidator implements Validator {
    @Override
    public boolean supports(Class<?> arg0) {
        return false;
    }

    @Override
    public void validate(Object uploadedFile, Errors error) {
        MultipartFile file = (MultipartFile) uploadedFile;

        if(file.isEmpty() || file.getSize()==0)
            error.rejectValue("file", "Empty", "please select a file");
        if(!(file.getContentType().toLowerCase().equals("application/pdf")
                || file.getContentType().toLowerCase().equals("application/msword")
                || file.getContentType().toLowerCase().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document"))){
            error.rejectValue("file", "Type", "only pdf or ms word file types are supported");
        }
        if(!file.isEmpty() && file.getSize()>10485760)
            error.rejectValue("file", "Size","maximum size is 10MB");
    }
}
