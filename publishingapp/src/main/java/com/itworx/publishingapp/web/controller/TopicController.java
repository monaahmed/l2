package com.itworx.publishingapp.web.controller;

import com.itworx.publishingapp.entity.Topic;
import com.itworx.publishingapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class TopicController {

    @Autowired
    private TopicService topicService;

    @GetMapping("/consumer/topics")
    public ModelAndView listTopics() {
        List<Topic> topics = topicService.getAllTopics();
        return new ModelAndView("topics").addObject("topics", topics);
    }

    @GetMapping("/publisher/topics")
    public ModelAndView listTopicsByPublisher() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Topic> topics = topicService.getTopicsByPublisher(username);
        return new ModelAndView("topics").addObject("topics", topics);
    }

}

