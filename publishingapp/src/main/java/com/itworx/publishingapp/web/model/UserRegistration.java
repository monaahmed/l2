package com.itworx.publishingapp.web.model;

import com.itworx.publishingapp.entity.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

public class UserRegistration {

	@NotEmpty
	@Email
	private String username;

	@NotEmpty
	private String password;

	private String role = Role.CONSUMER.name();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserRegistration that = (UserRegistration) o;
		return Objects.equals(username, that.username);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username);
	}

	@Override
	public String toString() {
		return "UserRegistration{" +
				"username='" + username + '\'' +
				'}';
	}
}
