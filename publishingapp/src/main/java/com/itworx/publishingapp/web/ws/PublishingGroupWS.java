package com.itworx.publishingapp.web.ws;

import com.itworx.publishingapp.entity.Document;
import com.itworx.publishingapp.entity.Topic;
import com.itworx.publishingapp.service.DocumentService;
import com.itworx.publishingapp.service.TopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/ws")
public class PublishingGroupWS {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private TopicService topicService;

    private static final Logger logger = LoggerFactory.getLogger(PublishingGroupWS.class);

    @GetMapping(path="/documents")
    public List<Document> getDocumentHierarchy() {
       return documentService.getAllDocuments();
    }

    @GetMapping(path="/document/{id}")
    public void getDocumentContentById(HttpServletResponse response, @PathVariable Long id) {
        Optional<Document> document = documentService.getDocumentDetails(id);
        response.setHeader("Content-Type", document.get().getContentType());
        response.setHeader("Content-Disposition", "attachment;filename=\"" + document.get().getFileName());
        try {
            response.getOutputStream().write(document.get().getContent());
            response.flushBuffer();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @GetMapping(path="/topics")
    public List<Topic> getTopics() {
        return topicService.getAllTopics();
    }
}