package com.itworx.publishingapp.web.model;

import com.itworx.publishingapp.entity.Topic;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class UploadedDocument{
    private Long id;

    @NotEmpty
    @Size(min=2, max=100)
    private String title;

    @Size(max=200)
    private String description;

    private MultipartFile file;

    private String fileName;

    private byte[] content;

    private String contentType;

    private String publisher;

    private List<Topic> topics = new ArrayList<>();

    @NotEmpty
    private List<String> selectedTopics = new ArrayList<>(); //selected topics ids

    public UploadedDocument() {
    }

    public UploadedDocument(Long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void addTopic(Topic topic){
        this.topics.add(topic);
    }

    public List<String> getSelectedTopics() {
        return selectedTopics;
    }

    public void setSelectedTopics(List<String> selectedTopics) {
        this.selectedTopics = selectedTopics;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName(){
        return file == null? this.fileName : file.getOriginalFilename();
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public byte[] getContent() throws IOException {
        return file == null? this.content : file.getBytes();
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType(){
        return file == null? this.contentType : file.getContentType();
    }


    public boolean isSelectedTopic(Topic topic) {
        return topics.contains(topic);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UploadedDocument that = (UploadedDocument) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "UploadedDocument{" +
                "title='" + title + '\'' +
                '}';
    }
}

