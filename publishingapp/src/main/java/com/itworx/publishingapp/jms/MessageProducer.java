package com.itworx.publishingapp.jms;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Topic;
import java.util.Set;

@Service
public class MessageProducer {

    @Autowired
    private JmsTemplate jmsTemplate;

    public void produce(final String message, final Set<com.itworx.publishingapp.entity.Topic> docTopics){
        docTopics.stream().forEach((com.itworx.publishingapp.entity.Topic t) -> jmsTemplate.convertAndSend(
                (new ActiveMQTopic("topic_" + Long.toString(t.getId()))), message)
        );
    }
}
