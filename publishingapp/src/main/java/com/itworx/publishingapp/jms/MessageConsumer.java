package com.itworx.publishingapp.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class MessageConsumer {

    @JmsListener(destination = "topic_2")
    public void consume(String message){
        System.out.println("Topic1 >>> " + message);
    }

}
