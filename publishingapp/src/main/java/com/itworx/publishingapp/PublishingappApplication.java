package com.itworx.publishingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PublishingappApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublishingappApplication.class, args);
	}
}
