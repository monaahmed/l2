package com.itworx.publishingapp.entity;

public enum Role {
    PUBLISHER,
    CONSUMER
}
