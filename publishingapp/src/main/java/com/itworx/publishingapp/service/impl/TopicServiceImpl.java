package com.itworx.publishingapp.service.impl;

import com.itworx.publishingapp.entity.Topic;
import com.itworx.publishingapp.repository.TopicRepository;
import com.itworx.publishingapp.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicRepository topicRepository;

    @Override
    public List<Topic> getAllTopics() {
        return topicRepository.findAllTopics();
    }

    @Override
    public List<Topic> getTopicsByPublisher(String email) {
        return topicRepository.findTopicsByPublisher(email);
    }

    @Override
    public Optional<Topic> getTopicById(Long id) {
        return topicRepository.findById(id);
    }

    @Override
    public String getTopicNameById(Long id) {
        return topicRepository.findById(id).get().getName();
    }
}
