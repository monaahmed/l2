package com.itworx.publishingapp.service;

import com.itworx.publishingapp.entity.Document;
import org.springframework.security.access.annotation.Secured;

import java.util.List;
import java.util.Optional;

public interface DocumentService {
    void publish(Document document);
    List<Document> getDocumentsByTopic(Long topicId);
    List<Document> getDocumentsByTopicAndPublisher(Long topicId, String email);
    List<Document> getAllDocuments();
    Optional<Document> getDocumentDetails(Long documentId);
    boolean existsByTitle(String title);
    void deleteDocument(Long documentId);
    void updateDocument(Document document);
}
