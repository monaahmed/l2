package com.itworx.publishingapp.service;

import com.itworx.publishingapp.entity.Topic;

import java.util.List;
import java.util.Optional;

public interface TopicService {
    List<Topic> getAllTopics();
    List<Topic> getTopicsByPublisher(String email);
    Optional<Topic> getTopicById(Long id);
    String getTopicNameById(Long id);
}
