package com.itworx.publishingapp.service.impl;

import com.itworx.publishingapp.entity.Document;
import com.itworx.publishingapp.jms.MessageProducer;
import com.itworx.publishingapp.repository.DocumentRepository;
import com.itworx.publishingapp.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private MessageProducer producer;

    @Override
    public void publish(Document document) {
        documentRepository.save(document);
        producer.produce(document.getTitle() + " has been published.",
                document.getTopics());
    }

    @Override
    public Optional<Document> getDocumentDetails(Long documentId) {
        return documentRepository.findById(documentId);
    }

    @Override
    public boolean existsByTitle(String title) {
        return documentRepository.existsByTitle(title);
    }

    @Override
    public List<Document> getDocumentsByTopic(Long topicId) {
        return documentRepository.findDocumentsByTopic(topicId);
    }

    @Override
    public List<Document> getDocumentsByTopicAndPublisher(Long topicId, String email) {
        return documentRepository.findDocumentsByTopicAndPublisher(topicId, email);
    }

    @Override
    public List<Document> getAllDocuments() {
        return documentRepository.findAll();
    }

    @Override
    public void deleteDocument(Long documentId) {
        documentRepository.deleteById(documentId);
    }

    @Override
    public void updateDocument(Document document) {
        producer.produce(document.getTitle() + " has been updated.",
                document.getTopics());
        documentRepository.save(document);
    }
}
