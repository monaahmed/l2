package com.itworx.publishingapp.service;

import com.itworx.publishingapp.entity.User;

public interface UserService {
    void register(User user);
    boolean existsByEmail(String username);
}
