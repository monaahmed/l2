import entity.ReservationRequest;
import entity.TimeSlot;
import strategy.SearchStrategy;
import strategy.impl.ListSearch;
import strategy.impl.MapSearch;

import java.time.LocalDate;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args){

        ReservationRequest newRequest = new ReservationRequest(LocalDate.now(),
                new TimeSlot(LocalTime.now(), 1), 6);

        SearchStrategy strategy = new ListSearch();

        System.out.println("Using List:");
        long startTime = System.currentTimeMillis();
        boolean available = strategy.checkAvailability(newRequest);
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);
        System.out.println("Available: " + available);
        System.out.println("\tTime taken: " + duration + " millis.");


        strategy = new MapSearch();
        System.out.println("Using Map:");
        startTime = System.currentTimeMillis();
        available = strategy.checkAvailability(newRequest);
        endTime = System.currentTimeMillis();

        duration = (endTime - startTime);
        System.out.println("Available: " + available);
        System.out.println("\tTime taken: " + duration + " millis.");
    }

}
