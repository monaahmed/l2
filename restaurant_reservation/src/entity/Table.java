package entity;

import java.util.ArrayList;
import java.util.List;

public class Table {
    private String code;
    private int size;

    private List<ReservationRequest> reservationRequests = new ArrayList<>();

    public Table(String code, int size) {
        this.code = code;
        this.size = size;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<ReservationRequest> getReservationRequests() {
        return reservationRequests;
    }

    public void setReservationRequests(List<ReservationRequest> reservationRequests) {
        this.reservationRequests = reservationRequests;
    }
}
