package entity;

import java.time.LocalDate;

public class ReservationRequest {
    private LocalDate date;
    private TimeSlot slot;
    private int number;

    public ReservationRequest(LocalDate date, TimeSlot slot, int number) {
        this.date = date;
        this.slot = slot;
        this.number = number;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public TimeSlot getSlot() {
        return slot;
    }

    public void setSlot(TimeSlot slot) {
        this.slot = slot;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}