package entity;

import java.time.LocalTime;

public class TimeSlot {
    private LocalTime start;
    private int duration;

    public TimeSlot(LocalTime start, int duration) {
        this.start = start;
        this.duration = duration;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
