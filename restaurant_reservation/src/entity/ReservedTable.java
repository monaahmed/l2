package entity;

public class ReservedTable {
    private String code;
    private TimeSlot slot;

    public ReservedTable(String code, TimeSlot slot) {
        this.code = code;
        this.slot = slot;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public TimeSlot getSlot() {
        return slot;
    }

    public void setSlot(TimeSlot slot) {
        this.slot = slot;
    }
}