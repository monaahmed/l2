package strategy.impl;

import entity.ReservationRequest;
import entity.Table;
import entity.TimeSlot;
import strategy.SearchStrategy;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ListSearch implements SearchStrategy {

    List<Table> tables;

    public ListSearch(){
        tables = initializeList();
    }

    @Override
    public boolean checkAvailability(ReservationRequest newRequest) {
        boolean reserved = false;
        for(Table t : tables){
            if(t.getSize() == newRequest.getNumber()) {
                reserved = false;
                for (ReservationRequest r : t.getReservationRequests()) {
                    if(r.getDate().equals(newRequest.getDate())){
                        reserved = true;
                    }
                }
                if(!reserved) return true;
            }
        }
        return false;
    }

    public List<Table> initializeList() {
        List<Table> tables = new ArrayList<>();
        Table t1 = new Table("A", 2);
        Table t2 = new Table("B", 4);
        Table t3 = new Table("C", 4);
        Table t4 = new Table("D", 6);
        Table t5 = new Table("E", 6);

        for(int i=0;i<10000;i++){
            t1.getReservationRequests().add(new ReservationRequest(LocalDate.now(), new TimeSlot(LocalTime.now(), 1), 2));
            t2.getReservationRequests().add(new ReservationRequest(LocalDate.now(), new TimeSlot(LocalTime.now(), 2), 4));
            t4.getReservationRequests().add(new ReservationRequest(LocalDate.now(), new TimeSlot(LocalTime.now(), 4), 6));
        }

        tables.add(t1);
        tables.add(t2);
        tables.add(t3);
        tables.add(t4);
        tables.add(t5);

        return tables;
    }
}
