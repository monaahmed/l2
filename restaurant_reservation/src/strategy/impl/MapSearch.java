package strategy.impl;

import entity.ReservationRequest;
import entity.ReservedTable;
import entity.TimeSlot;
import strategy.SearchStrategy;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapSearch implements SearchStrategy {

    private Map<Integer, Map<LocalDate, List<ReservedTable>>> reservations;

    public MapSearch() {
        reservations = initializeMap();
    }

    @Override
    public boolean checkAvailability(ReservationRequest newRequest) {
        boolean reserved = false;
        Map<LocalDate, List<ReservedTable>> days = reservations.get(newRequest.getNumber());

        List<ReservedTable> tables = days.get(newRequest.getDate());
        if (tables == null) return true;

        for (ReservedTable t : tables) {
            if (newRequest.getSlot().getStart().equals(t.getSlot().getStart())) {
                reserved = true;
            }
        }
        return !reserved;
    }

    /** Just for Testing. It shouldn't be this way in real life **/
    private Map<Integer, Map<LocalDate, List<ReservedTable>>> initializeMap() {
        Map<Integer, Map<LocalDate, List<ReservedTable>>> map = new HashMap<>();

        for (int i = 2; i < 7; i += 2) {
            Map<LocalDate, List<ReservedTable>> dates = new HashMap<>();
            for (int j = 0; j < 10000; j++) {
                List<ReservedTable> reservedTables = new ArrayList<>();
                for (int k = 0; k < 3; k++) {
                    reservedTables.add(new ReservedTable(String.valueOf((char) ('A' + k)), new TimeSlot(LocalTime.now().plusHours(k), 1)));
                }
                dates.put(LocalDate.now().plusDays(j), reservedTables);
            }
            map.put(i, dates);
        }

        return map;
    }
}