package strategy;

import entity.ReservationRequest;

public interface SearchStrategy {
    boolean checkAvailability(ReservationRequest newRequest);
}
